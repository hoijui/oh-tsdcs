TsDC-questionnaere
===

> First we ask the following questions!

# How many parts does you construction roughly have?
- 1
- -10
- -100
- more than hunderd

# What technologies does it contain?
- mechanical
- electronic
- textile
- glueing
- welding
- firmware
- software
- hydraulic
- pneumatic

# Did you construct all parts by yourself? If not, how are the parts licensed?
- Proprietary
- Open Source Hardware
- Open Source Software
- Everything is self made!

> Now we can give them overview what they have to deliver for what technology and what kind of license. We should add something like:

## A part from these requirements please ask yourself:
Are there special features in the production process the others should know about or mistakes you could save the rebuilders from?

Thanks for taking this questionniare and good luck with OpenSourcing your product, the world will apreciate it, if it's a good one. ;)
