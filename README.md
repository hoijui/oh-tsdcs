# README for DIN SPEC 3105 TsDCs
Bad change here!
## Brief Description

Technology-specific Documentation Criteria (TsDC) specify the requirements for the technical documentation of Open Source Hardware (OSH). A TsDC is created (yet manually) by OSH projects/developers and is a subset of the TsDC database (TsDC-DB) provided in this repository.
The concept of a TsDC was initially mentioned in DIN SPEC 3105-1 (since v0.3) and probably will be mainly used in this context.

## Related Standards

- [DIN SPEC 3105-1 v0.9](https://gitlab.com/OSEGermany/ohs)
- [DIN SPEC 3105-2 v0.9](https://gitlab.com/OSEGermany/ohs)
- [The Open Know-How Manifest Specification Version 1.0](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/src/branch/master/1)

## Covered Perspectives

### "Maker"

**In one word: build and operate at your own risk.**

Certified technical documentation shall deliver sufficient information to enable (at least) professionals to reproduce, operate, maintain and dispose the documented hardware. Thus this hardware:

- is not meant to be (commercially) distributed _without_ previous professional inspection;
- is meant to run in uncritical environments regarding saftey.

## Requirements for TsDC and this database

- human-readable (and thus -maintainable)
- compatible with graph databases

## Generated content

Graph representations (logical and visual) of the RDF part of the standard
are generated after each commit to this repo,
and are stored [here](https://osegermany.gitlab.io/oh-tsdcs/).
