# TsDC DB

The info in this file is to be converted into RDF files,
which will make this file obsolete.
Work on this already started in the various _*.ttl_ files in this repo.

## component

### manufactured

required information:

- geometry
- material properties
- tolerances
- surface specifications
- manufacturing specifications (e.g. manufacturing process, machine settings) [if technically necessary]

common source:

- technical drawing
- CAD model
- manufacturing instructions [if technically necessary]
- additional manufacturing files (e.g. STL files for additive manufacturing processes) [if technically necessary]

### proprietary

required information:

- relevant specifications

common source:

- datasheet or unambiguous reference
- explanations about the choice
- list of dependencies (e.g. additional hardware/software)

### OSH

required information:

- unambiguous reference to the corresponding release of the technical documentation
- certificate (OSHWA/DIN SPEC 3105)

common source:

- manifest file (see [related standards](README.md#related-standards))

## assembly

required information:

- geometry
- manufacturing technology
- involved components
- junctions
- assembly instructions

common source:

- assembly drawing
- bill of materials
- manufacturing plan
- dismantling plan (Operation & Maintenance)

### manufacturing technology [if technically necessary]

#### mechanical joining

required information:

- preloads (e.g. tightening torque)
- calibration
- specialized tools
- additional materials (e.g. lubricants)

common source:

- assembly drawing
- calibration plan
- list of specialized tools and additional materials

#### welding

required information:

- welding seam geometry (including welding seam preparation)
- filler material
- welding process
- welding sequence

common source:

- assembly drawing
- welding sequence plan
- welding specifications
- welding test documents

#### soldering

required information:

- temperature range
- filler material
- surface preparation

common source:

- assembly drawing
- soldering instructions

#### adhesive bonding

required information:

- glue
- solidification requirements
  - temperature/light
  - setting time
- processing information
  - surface preparation
  - spray/bead/plane application
  - pot life

common source:

- datasheet
- bonding instructions

## post-processing

### coating/filling

required information:

- coating/filling technology
- coating/filling specifications
- additional materials
- involved components/assemblies
- involved areas
- manufacturing instructions [if technically necessary]
- specialized tools [if technically necessary]

common source:

- technical drawing
- processing guidelines [if technically necessary]
- list of specialized tools  [if technically necessary]

### change of material properties

required information:

- processing technology
- targeted material properties
- involved components/assemblies
- involved areas
- manufacturing instructions [if technically necessary]
- additional materials [if technically necessary]
- specialized tools [if technically necessary]

common source:

- technical drawing
- processing guidelines [if technically necessary]
- list of specialized tools and additional materials [if technically necessary]

### IT setup

#### operating system/firmware

required information:

- unambiguous reference to the corresponding release
- dependencies (e.g. additional hardware/software)
- license
- flashing instructions (including programmer information)

common source:

- URL (to the source code)
- list of dependencies
- flashing instructions

#### software

required information:

- unambiguous reference to the corresponding release
- system requirements (e.g. hardware, operating system/firmware, libraries, drivers)
- installation process [if technically necessary]

common source:

- URL (points to the source code)
- list of system requirements/dependencies
- installation instructions [if technically necessary]

## interaction

### internal

#### electro-magnetic

required information:

- interconnection of functional components

common source:

- circuit/wiring diagram

#### hydraulic

required information:

- interconnection of functional components

common source:

- hydraulic plan

#### pneumatic

required information:

- interconnection of functional components

common source:

- pneumatic plan

### external

#### boundary condition

required information:

- physical category (electro-magnetic, mechanic, thermal etc.)
- type (static, dynamic, random)
- value (range)

common source:

- calculation/description

#### emission

##### intended

required information:

- function
- physical category (electro-magnetic, mechanic, thermal etc.)
- type (static, dynamic, random)
- value (range)

common source:

- calculation/description

##### side effect

required information:

- physical category (electro-magnetic, mechanic, thermal etc.)
- type (static, dynamic, random)
- value (range)

common source:

- calculation/description
- measurement results

## superordinate document

### operation instructions

### testing operation
